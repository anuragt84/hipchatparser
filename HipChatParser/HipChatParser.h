//
//  HipChatParser.h
//  HipChatParser
//
//  Created by Anurag Tolety on 2/27/15.
//  Copyright (c) 2015 Anurag Tolety. All rights reserved.
//

#import <Foundation/Foundation.h>

#define EMOTICONS_KEY @"emoticons"
#define MENTIONS_KEY @"mentions"
#define LINKS_KEY @"links"
#define URL_KEY @"url"
#define TITLE_KEY @"title"

@interface HipChatParser : NSObject

@property (nonatomic, copy) NSString* input;

- (void)jsonStringRepresentationOfInputWithCompletionBlock:(void (^)(NSError* error, NSString* output))completionBlock;

@end
