//
//  HipChatParser.m
//  HipChatParser
//
//  Created by Anurag Tolety on 2/27/15.
//  Copyright (c) 2015 Anurag Tolety. All rights reserved.
//

#import "HipChatParser.h"
#import <AFNetworking.h>
#import <HTMLReader.h>
#import <HTMLTextNode.h>
#import <SBJson4.h>

@interface HipChatParser ()

@property (nonatomic, strong) NSString* output;
@property (strong, nonatomic) NSRegularExpression* usernameRegex;
@property (strong, nonatomic) NSRegularExpression* emoticonRegex;
@property (strong, nonatomic) NSRegularExpression* urlRegex;

@end
@implementation HipChatParser

- (NSRegularExpression*)usernameRegex
{
    if (!_usernameRegex) {
        _usernameRegex = [[NSRegularExpression alloc] initWithPattern:@"@([a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)" options:0 error:nil];
    }
    return _usernameRegex;
}

- (NSRegularExpression*)emoticonRegex
{
    if (!_emoticonRegex) {
        _emoticonRegex = [[NSRegularExpression alloc] initWithPattern:@"\\(([a-zA-Z0-9!@#$%&'*+/=?^_`{|}~-]+)\\)" options:0 error:nil];
    }
    return _emoticonRegex;
}

- (NSRegularExpression*)urlRegex
{
    if (!_urlRegex) {
        _urlRegex = [[NSRegularExpression alloc] initWithPattern:@"https{0,1}://([a-zA-Z0-9!@#$%&'*+/=?^_`{|}\\.~-]+)\\.([a-zA-Z0-9!@#$%&'*+/=?^_`{|}\\.~-]+)" options:0 error:nil];
    }
    return _urlRegex;
}

/*!
 * @discussion Fetches the json representation of the input string by finding out mentions, emoticons, urls and their titles
 * @param completionBlock executed on fetching all the data and converting to json
 */
- (void)jsonStringRepresentationOfInputWithCompletionBlock:(void (^)(NSError* error, NSString* output))completionBlock {
    if (!self.input || [self.input isEqualToString:@""]) {
        NSError* error = [[NSError alloc] initWithDomain:nil code:0 userInfo:@{NSLocalizedDescriptionKey : @"input is empty"}];
        completionBlock(error, nil);
        return;
    }
    // We first process everything into a dictionary and then generate the json from it
    NSMutableDictionary* outputDictionary = [[NSMutableDictionary alloc] init];
    
    // Allocate arrays to gather all the matches
    NSMutableArray* usernameMatches = [[NSMutableArray alloc] init];
    NSMutableArray* emoticonMatches = [[NSMutableArray alloc] init];
    NSMutableArray* urlMatches = [[NSMutableArray alloc] init];
    
    // Find username matches
    NSArray* matches = [self.usernameRegex matchesInString:self.input options:0 range:NSMakeRange(0, self.input.length)];
    for (NSTextCheckingResult* match in matches) {
        NSString* matchText = [self.input substringWithRange:[match range]];
        [usernameMatches addObject:[matchText substringFromIndex:1]];
    }
    if ([usernameMatches count]) {
        [outputDictionary setObject:usernameMatches forKey:MENTIONS_KEY];
    }
    
    // Find emoticon matches
    matches = [self.emoticonRegex matchesInString:self.input options:0 range:NSMakeRange(0, self.input.length)];
    for (NSTextCheckingResult* match in matches) {
        NSString* matchText = [self.input substringWithRange:[match range]];
        [emoticonMatches addObject:[matchText substringWithRange:NSMakeRange(1, matchText.length - 2)]];
    }
    if ([emoticonMatches count]) {
        [outputDictionary setObject:emoticonMatches forKey:EMOTICONS_KEY];
    }
    
    // Find url matches and retrieve titles by making http requests and parsing the output through HTML Reader
    matches = [self.urlRegex matchesInString:self.input options:0 range:NSMakeRange(0, self.input.length)];
    for (NSTextCheckingResult* match in matches) {
        NSString* matchText = [self.input substringWithRange:[match range]];
        [urlMatches addObject:matchText];
    }
    if ([urlMatches count]) {
        [self fetchTitlesForUrls:urlMatches withCompletionBlock:^(NSError* error, NSArray *results) {
            NSMutableArray* linkEntries = [[NSMutableArray alloc] init];
            for (int i = 0; i < [urlMatches count]; i++) {
                [linkEntries addObject:[NSDictionary dictionaryWithObjectsAndKeys:[urlMatches objectAtIndex:i], URL_KEY, [results objectAtIndex:i], TITLE_KEY, nil]];
            }
            [outputDictionary setObject:linkEntries forKey:LINKS_KEY];
            NSError* jsonGenerationError;
            // Even if the passed in error is not nil, we still attempt to generate json. This way we can return the mentions and emoticons even if we can't fetch the titles
            if ([self generateJsonFromDictionary:outputDictionary withError:&jsonGenerationError]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completionBlock(error, self.output);
                });
            } else {
                // Aggregate errors if json fails
                if (error) {
                    NSError* aggregatedError = [[NSError alloc] initWithDomain:nil code:0 userInfo:@{NSLocalizedDescriptionKey : [jsonGenerationError localizedDescription], NSUnderlyingErrorKey : error}];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completionBlock(aggregatedError, nil);
                    });
                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completionBlock(jsonGenerationError, nil);
                    });
                }
            }
        }];
    } else {
        // No url matches exist, so we generate the output json data directly
        NSError* jsonGenerationError;
        if ([self generateJsonFromDictionary:outputDictionary withError:&jsonGenerationError]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completionBlock(nil, self.output);
            });
        } else {
            NSError* jsonError = [[NSError alloc] initWithDomain:nil code:0 userInfo:@{NSLocalizedDescriptionKey : @"unable to generate json", NSUnderlyingErrorKey : jsonGenerationError}];
            dispatch_async(dispatch_get_main_queue(), ^{
                completionBlock(jsonError, nil);
            });
        }
    }
}

/*!
 * @discussion Makes requests to fetch the titles for all the urls passed in. On finishing, executes the completion block
 * @param titleFetchCompletionBlock executed on getting a success/failure on all the individual requests to fetch url titles
 */
- (void)fetchTitlesForUrls:(NSArray*)urlArray withCompletionBlock:(void (^)(NSError* error, NSArray* results))titlesFetchCompletionBlock
{
    // These values can get changed by the block
    __block NSError* latestError = nil;
    __block NSMutableArray* urlTitleResults = [[NSMutableArray alloc] init];
    
    // Add empty objects. Even if we can't fetch a title, we still put an empty string in the output dictionary
    for (int i = 0; i < [urlArray count]; i++) {
        [urlTitleResults addObject:@""];
    }
    // Create a dispatch group so that we know when all the requests are complete
    dispatch_group_t urlRequestGroup = dispatch_group_create();
    for (int i = 0; i < [urlArray count]; i++) {
        NSURL *URL = [NSURL URLWithString:[urlArray objectAtIndex:i]];
        NSURLRequest *request = [NSURLRequest requestWithURL:URL];
        dispatch_group_enter(urlRequestGroup);
        // User AFNetworking to make HTTP requests
        AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            
            // Use HTML Reader to get the title from the html
            HTMLDocument* htmlDocument = [HTMLDocument documentWithString:responseString];
            HTMLElement* element = [htmlDocument firstNodeMatchingSelector:@"title"];
            if (element && ([element.children count] == 1) && ([[element.children firstObject] class] == [HTMLTextNode class])) {
                HTMLTextNode* titleData = [element.children firstObject];
                NSLog(@"Title for index %d: %@", i, titleData.data);
                [urlTitleResults replaceObjectAtIndex:i withObject:titleData.data];
            }
            dispatch_group_leave(urlRequestGroup);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            latestError = error;
            dispatch_group_leave(urlRequestGroup);
        }];
        [op start];
    }
    // The block passed in here executes when all requests are complete
    dispatch_group_notify(urlRequestGroup, dispatch_get_main_queue(), ^{
        NSLog(@"all requests complete");
        titlesFetchCompletionBlock(latestError, urlTitleResults);
    });
}

/*!
 * @discussion Generates a json representation of the dictionary parameter using SBJSON4
 * @param outputDictionary A dictionary from which the JSON is to be generated
 * @return Yes or no depending on whether the json generation was successful or not
 */
- (BOOL)generateJsonFromDictionary:(NSDictionary*)outputDictionary withError:(NSError**)error {
    SBJson4Writer* jsonWriter = [[SBJson4Writer alloc] init];
    jsonWriter.humanReadable = YES;
    NSString* jsonOutput = [jsonWriter stringWithObject:outputDictionary];
    if (!jsonWriter.error) {
        self.output = jsonOutput;
        return YES;
    } else {
        self.output = nil;
        *error = [[NSError alloc] initWithDomain:nil code:0 userInfo:@{NSLocalizedDescriptionKey : jsonWriter.error}];
        return NO;
    }
}
@end
