//
//  ViewController.m
//  HipChatParser
//
//  Created by Anurag Tolety on 2/27/15.
//  Copyright (c) 2015 Anurag Tolety. All rights reserved.
//

#import "ViewController.h"
#import "HipChatParser.h"

@interface ViewController ()

@property (strong, nonatomic) HipChatParser* parser;

@end

@implementation ViewController

- (HipChatParser*)parser {
    if (!_parser) {
        _parser = [[HipChatParser alloc] init];
    }
    return _parser;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.parser.input = @"@john and @George released an awesome feature (hurray) (smiley) posting this on http://www.google.com and https://www.facebook.com";
    [self.parser jsonStringRepresentationOfInputWithCompletionBlock:^(NSError *error, NSString *output) {
        if (error) {
            NSLog(@"error: %@", [error localizedDescription]);
        } else {
            NSLog(@"success: %@", output);
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
