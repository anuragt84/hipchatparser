//
//  HipChatParserTests.m
//  HipChatParserTests
//
//  Created by Anurag Tolety on 2/27/15.
//  Copyright (c) 2015 Anurag Tolety. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "HipChatParser.h"
#import <SBJson4.h>

@interface HipChatParserTests : XCTestCase

@property (nonatomic, strong) HipChatParser* parser;

@end

@implementation HipChatParserTests

- (HipChatParser*)parser {
    if (!_parser) {
        _parser = [[HipChatParser alloc] init];
    }
    return _parser;
}

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

/*!
 * @discussion Checks if the expected values are same as the values found in the output
 * @param mentions array containing the username mentions
 * @param emoticons array containing the emoticons
 * @param links array containing the links (title and url)
 * @return YES if equivalent, NO if not
 */
- (BOOL)checkIfExpectedValuesWithMentions:(NSArray*)mentions emoticons:(NSArray*)emoticons links:(NSArray*)links areEquivalentToOutput:(NSString*)output {
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:[output dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
    BOOL mentionsAreEquivalent = NO;
    if (mentions) {
        mentionsAreEquivalent = [mentions isEqualToArray:[dictionary objectForKey:MENTIONS_KEY]];
    } else {
        if (![dictionary objectForKey:MENTIONS_KEY]) {
            mentionsAreEquivalent = YES;
        } else {
            mentionsAreEquivalent = NO;
        }
    }
    if (!mentionsAreEquivalent) {
        return NO;
    }
    BOOL emoticonsAreEquivalent = NO;
    if (emoticons) {
        emoticonsAreEquivalent = [emoticons isEqualToArray:[dictionary objectForKey:EMOTICONS_KEY]];
    } else {
        if (![dictionary objectForKey:EMOTICONS_KEY]) {
            emoticonsAreEquivalent = YES;
        } else {
            emoticonsAreEquivalent = NO;
        }
    }
    if (!emoticonsAreEquivalent) {
        return NO;
    }
    if ([links count] == [[dictionary objectForKey:LINKS_KEY] count]) {
        for (int i = 0; i < [links count]; i++) {
            NSDictionary* expectedLink = [links objectAtIndex:i];
            NSDictionary* resultLink = [[dictionary objectForKey:LINKS_KEY] objectAtIndex:i];
            if (!([[expectedLink objectForKey:URL_KEY] isEqualToString:[resultLink objectForKey:URL_KEY]] &&  [[expectedLink objectForKey:TITLE_KEY] isEqualToString:[resultLink objectForKey:TITLE_KEY]])) {
                return NO;
            }
        }
        return YES;
    } else {
        return NO;
    }
}

#pragma mark AtlassianExamples

- (void)testAtlassianExample0 {
    self.parser.input = @"@chris you around?";
    NSArray* expectedMentions = [NSArray arrayWithObjects:@"chris", nil];
    XCTestExpectation *parserResultExpectation = [self expectationWithDescription:@"ParserResultCorrect"];
    [self.parser jsonStringRepresentationOfInputWithCompletionBlock:^(NSError* error, NSString *output) {
        XCTAssert(!error && output && [self checkIfExpectedValuesWithMentions:expectedMentions emoticons:nil links:nil areEquivalentToOutput:output]);
        [parserResultExpectation fulfill];
    }];
    [self waitForExpectationsWithTimeout:10 handler:nil];
}

- (void)testAtlassianExample1 {
    self.parser.input = @"Olympics are starting soon; http://www.nbcolympics.com";
    NSArray* expectedLinks = [NSArray arrayWithObjects:@{URL_KEY : @"http://www.nbcolympics.com", TITLE_KEY : @"NBC Olympics | Home of the 2016 Olympic Games in Rio"}, nil];
    XCTestExpectation *parserResultExpectation = [self expectationWithDescription:@"ParserResultCorrect"];
    [self.parser jsonStringRepresentationOfInputWithCompletionBlock:^(NSError* error, NSString *output) {
        XCTAssert(!error && output && [self checkIfExpectedValuesWithMentions:nil emoticons:nil links:expectedLinks areEquivalentToOutput:output]);
        [parserResultExpectation fulfill];
    }];
    [self waitForExpectationsWithTimeout:10 handler:nil];
}

- (void)testAtlassianExample2 {
    self.parser.input = @"@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016";
    NSArray* expectedMentions = [NSArray arrayWithObjects:@"bob", @"john", nil];
    NSArray* expectedEmoticons = [NSArray arrayWithObjects:@"success", nil];
    NSArray* expectedLinks = [NSArray arrayWithObjects:@{URL_KEY : @"https://twitter.com/jdorfman/status/430511497475670016", TITLE_KEY : @"Justin Dorfman on Twitter: \"nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq\""}, nil];
    XCTestExpectation *parserResultExpectation = [self expectationWithDescription:@"ParserResultCorrect"];
    [self.parser jsonStringRepresentationOfInputWithCompletionBlock:^(NSError* error, NSString *output) {
        XCTAssert(!error && output && [self checkIfExpectedValuesWithMentions:expectedMentions emoticons:expectedEmoticons links:expectedLinks areEquivalentToOutput:output]);
        [parserResultExpectation fulfill];
    }];
    [self waitForExpectationsWithTimeout:10 handler:nil];
}

- (void)testAtlassianExample3 {
    self.parser.input = @"Good morning! (megusta) (coffee)";
    NSArray* expectedEmoticons = [NSArray arrayWithObjects:@"megusta", @"coffee", nil];
    XCTestExpectation *parserResultExpectation = [self expectationWithDescription:@"ParerResultCorrect"];
    [self.parser jsonStringRepresentationOfInputWithCompletionBlock:^(NSError* error, NSString *output) {
        XCTAssert(!error && output && [self checkIfExpectedValuesWithMentions:nil emoticons:expectedEmoticons links:nil areEquivalentToOutput:output]);
        [parserResultExpectation fulfill];
    }];
    [self waitForExpectationsWithTimeout:10 handler:nil];
}

#pragma mark MentionsTests

- (void)testMentionsContinuous {
    self.parser.input = @"@chris@Graf you around?";
    NSArray* expectedMentions = [NSArray arrayWithObjects:@"chris", @"Graf", nil];
    XCTestExpectation *parserResultExpectation = [self expectationWithDescription:@"ParserResultCorrect"];
    [self.parser jsonStringRepresentationOfInputWithCompletionBlock:^(NSError* error, NSString *output) {
        XCTAssert(!error && output && [self checkIfExpectedValuesWithMentions:expectedMentions emoticons:nil links:nil areEquivalentToOutput:output]);
        [parserResultExpectation fulfill];
    }];
    [self waitForExpectationsWithTimeout:10 handler:nil];
}

- (void)testMentionsWithSpecialCharacters {
    self.parser.input = @"@chris_1? @Graf-2~@Cody! you around?";
    NSArray* expectedMentions = [NSArray arrayWithObjects:@"chris_1?", @"Graf-2~", @"Cody!", nil];
    XCTestExpectation *parserResultExpectation = [self expectationWithDescription:@"ParserResultCorrect"];
    [self.parser jsonStringRepresentationOfInputWithCompletionBlock:^(NSError* error, NSString *output) {
        XCTAssert(!error && output && [self checkIfExpectedValuesWithMentions:expectedMentions emoticons:nil links:nil areEquivalentToOutput:output]);
        [parserResultExpectation fulfill];
    }];
    [self waitForExpectationsWithTimeout:10 handler:nil];
}

- (void)testMentionsSkipEmpty {
    self.parser.input = @"@ @Graf@Cody you around?";
    NSArray* expectedMentions = [NSArray arrayWithObjects:@"Graf", @"Cody", nil];
    XCTestExpectation *parserResultExpectation = [self expectationWithDescription:@"ParserResultCorrect"];
    [self.parser jsonStringRepresentationOfInputWithCompletionBlock:^(NSError* error, NSString *output) {
        XCTAssert(!error && output && [self checkIfExpectedValuesWithMentions:expectedMentions emoticons:nil links:nil areEquivalentToOutput:output]);
        [parserResultExpectation fulfill];
    }];
    [self waitForExpectationsWithTimeout:10 handler:nil];
}

#pragma mark EmoticonsTests

- (void)testEmoticonsContinuous {
    self.parser.input = @"(success)(megusta)(coffee) you around?";
    NSArray* expectedEmoticons = [NSArray arrayWithObjects:@"success", @"megusta", @"coffee", nil];
    XCTestExpectation *parserResultExpectation = [self expectationWithDescription:@"ParserResultCorrect"];
    [self.parser jsonStringRepresentationOfInputWithCompletionBlock:^(NSError* error, NSString *output) {
        XCTAssert(!error && output && [self checkIfExpectedValuesWithMentions:nil emoticons:expectedEmoticons links:nil areEquivalentToOutput:output]);
        [parserResultExpectation fulfill];
    }];
    [self waitForExpectationsWithTimeout:10 handler:nil];
}

- (void)testEmoticonsSkipEmpty {
    self.parser.input = @"() (megusta) ()(coffee) you around?";
    NSArray* expectedEmoticons = [NSArray arrayWithObjects:@"megusta", @"coffee", nil];
    XCTestExpectation *parserResultExpectation = [self expectationWithDescription:@"ParserResultCorrect"];
    [self.parser jsonStringRepresentationOfInputWithCompletionBlock:^(NSError* error, NSString *output) {
        XCTAssert(!error && output && [self checkIfExpectedValuesWithMentions:nil emoticons:expectedEmoticons links:nil areEquivalentToOutput:output]);
        [parserResultExpectation fulfill];
    }];
    [self waitForExpectationsWithTimeout:10 handler:nil];
}

- (void)testEmoticonsWithSpecialCharacters {
    self.parser.input = @"(success_1?) (megusta-2~)(coffee_3!) you around?";
    NSArray* expectedEmoticons = [NSArray arrayWithObjects:@"success_1?", @"megusta-2~", @"coffee_3!", nil];
    XCTestExpectation *parserResultExpectation = [self expectationWithDescription:@"ParserResultCorrect"];
    [self.parser jsonStringRepresentationOfInputWithCompletionBlock:^(NSError* error, NSString *output) {
        XCTAssert(!error && output && [self checkIfExpectedValuesWithMentions:nil emoticons:expectedEmoticons links:nil areEquivalentToOutput:output]);
        [parserResultExpectation fulfill];
    }];
    [self waitForExpectationsWithTimeout:10 handler:nil];
}
@end
