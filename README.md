Readme

Open Source code used through Cocoa Pods:

1. AFNetworking - For url requests
2. HTMLReader - For parsing HTML
3. SBJSON4 - For generating json from an NSDictionary 

Code Overview

1. Using the parser involves setting an input and passing a completion block to one of its method.
2. Uses regexes to match mentions, emoticons and urls. 
3. Assumes all mentions that start with @ are valid mentions. Actual validation can be implemented by comparing against a locally stored list. 
4. Assumes all emoticons with (name) pattern are valid emoticons.
5. Uses dispatch groups to fetch titles of url by making use of AFNetworking. Used dispatch groups since we need to generate json after all the requests are complete. This avoids nested blocks.
6. Uses HTMLReader to find the title of the document.
7. Uses SBJSON4 to generate json. Could have gone with NSJsonSerialization, but that produced extra escape characters for the forward slashes in the url 

Testing

1. Some basic testing with the examples Amanda gave me along with some of my own.